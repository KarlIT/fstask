﻿using System;
using System.Net;
using System.Net.Sockets;
using System.IO;
using System.Text;
public class ClientSocket1
{
    static void Main(string[] args)
    {
        byte[] bytes = new byte[1024];

        try
        {
            // Connect to a server
            Console.WriteLine("\nPlease enter the address to connect to: \n");  
            var host = Console.ReadLine();
            IPAddress ipAddress = IPAddress.Parse(host);
            var port = 1234;

            // Create a TCP/IP  socket.    
            Socket socket = new Socket(ipAddress.AddressFamily,
                SocketType.Stream, ProtocolType.Tcp);

            // Connect the socket to the remote endpoint. Catch any errors.    
            try
            {
                // Connect to Remote EndPoint  
                socket.Connect(host, port);
                // Check if the connection is still Alive
                bool blockingState = socket.Blocking;
                Console.WriteLine("\nconnected to {0}\n",socket.RemoteEndPoint.ToString());

                // Encode the data string into a byte array.    
                byte[] msg = Encoding.ASCII.GetBytes("I am listening now");

                // Send the data through the socket.    
                int bytesSent = socket.Send(msg);
                while (blockingState)
                {
                    // Receive the response from the remote device.    
                    int bytesRec = socket.Receive(bytes);
                    Console.WriteLine("Server: {0}",
                        Encoding.ASCII.GetString(bytes, 0, bytesRec));

                }
                Console.WriteLine("Socket is still connected {0}", blockingState.ToString());
                // Release the socket.    
                socket.Shutdown(SocketShutdown.Both);
                socket.Close();

            }
            catch (ArgumentNullException ane)
            {
                Console.WriteLine("ArgumentNullException : {0}", ane.ToString());
            }
            catch (SocketException se)
            {
                Console.WriteLine("SocketException : {0}", se.ToString());
            }
            catch (Exception e)
            {
                Console.WriteLine("Unexpected exception : {0}", e.ToString());
            }
        }
        catch (Exception e)
        {
            Console.WriteLine(e.ToString());
        }

    }
}