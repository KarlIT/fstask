Getting started:

	Download files from git repository:
	1) Open your Bash or cmd and navigte to directory where do you want to store the files
		2) insert command  "git clone https://gitlab.com/KarlIT/fstask.git"
		
	To run docker images/apps
		1) navigate to "\fstask\images" folder (In the freshly downloaded directory). 
		2) insert command "docker load --input server.tar" - NB! if you have any issues and you are using Bash, then try cmd/terminal/power shell.
		3) insert command "docker load --input client.tar"
		4) insert command "docker images" and make sure you have both images created
		5) insert command "docker run -ti server_image" - NB! IF you see warning then ignore it and wait for few seconds. You should see output "Initialising server....  Use -> "  172.0.0.2  " to connect. Waiting for incoming connections..."
		6) Open second console (cmd/terminal/power shell) and navigate to the same directory "~\fstask\images"  insert command "docker load --input client.tar" (Ignore the warning if any) You should see output "Please enter the address to connect to: "
		enter the address that Server showed in the step 5 (172.0.0.2).
		7) Now you can type into "Server" console and client will show that out in Clients  console.
