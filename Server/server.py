import time, socket

print("\nInitialising server....\n")
time.sleep(1)

s = socket.socket()
host = socket.gethostname()
ip = socket.gethostbyname(host)
port = 1234
s.bind((host, port))
print( "Use -> \" ",ip," \" to connect.\n")
           
s.listen(1)
print("Waiting for incoming connections...\n")
conn, addr = s.accept()
print("Received connection from ", addr[0], "(", addr[1], ")\n")

while True:
    message = input(str("Me: "))
    if message == "[e]":
        message = "Server down!"
        conn.send(message.encode())
        print("Server down")
        break
    conn.send(message.encode())